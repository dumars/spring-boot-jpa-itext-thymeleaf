# README #

Spring boot + jpa + itext + thymeleaf 練習用專案

### 環境需求 ###

* Java 11+
* Maven 3.5+
* Git
* Docker 2+

### Maven ###

Maven 指令參考

* mvn clean compile
* mvn spring-boot:run
* mvn clean package

### Docker ###

如何啟動 ms sql server?

進入專案中的 docker 資料夾並執行指令 `docker-compose up -d` ，啟動之後會在此資料夾下建立 /data 與 /log 來存放資料，密碼 1qaz@WSX 可視需要修改 docker-compose.yml，port 1433

參考指令：

* `docker-compose down` (或是 start, stop, restart)
* `docker ps` 或是 `docker ps -a`
* docker logs <container_name> 即 `docker logs mssql` 或是 `docker logs -f mssql`

#### Create Database ####

建立接下來要使用的 database(如果有變更密碼記得要更改指令中的密碼部分)

`docker exec -it mssql /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "1qaz@WSX" -Q "CREATE DATABASE spring"`


### 安裝lombok ###

如果不是使用Intellij IDEA，需要再安裝lombok套件。

1. 到[lombok官網](https://projectlombok.org/download)，下載jar。
1. 在命令提示字元執行java -jar lombok.jar，把lombok安裝在開發工具的路徑。

### How to run? ###

第一次執行時，需加入 profiles 參數，可以自動建立基本測試資料

`mvn spring-boot:run -Dspring-boot.run.profiles=init`

之後移除此設定避免錯誤

`mvn spring-boot:run`

`mvn spring-boot:run -Dspring-boot.run.profiles=thymeleaf`
