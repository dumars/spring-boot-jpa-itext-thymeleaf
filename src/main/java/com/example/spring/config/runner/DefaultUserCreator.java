package com.example.spring.config.runner;

import com.example.spring.entity.User;
import com.example.spring.entity.enumeration.Role;
import com.example.spring.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Profile("init")
@Slf4j
@Component
public class DefaultUserCreator implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) {
        createAdmin();
        createExternalVendor();
    }

    private void createAdmin() {
        log.debug("create admin now.");
        User admin = new User();
        admin.setUsername("admin");
        admin.setName("Administrator");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setRole(Role.ADMIN);
        admin.setDescription("System default administrator");
        userRepository.save(admin);
    }

    private void createExternalVendor() {
        log.debug("create loki now.");
        User vendor = new User();
        vendor.setUsername("loki");
        vendor.setName("Loki");
        vendor.setPassword(passwordEncoder.encode("loki"));
        vendor.setRole(Role.EXTERNAL_VENDOR);
        vendor.setDescription("God of mischief");
        userRepository.save(vendor);
    }
}
