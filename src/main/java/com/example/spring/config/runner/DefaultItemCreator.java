package com.example.spring.config.runner;

import com.example.spring.entity.Item;
import com.example.spring.entity.enumeration.ItemStatus;
import com.example.spring.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Profile("init")
@Slf4j
@Component
public class DefaultItemCreator implements CommandLineRunner {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public void run(String... args) {
        IntStream.range(1, 200).forEach(i -> {
            Item item = new Item();
            item.setName("Demo Item " + i);
            item.setCostPrice(RandomUtils.nextInt(100, 200));
            item.setPrice(RandomUtils.nextInt(201, 300));

            if (i < 8)
                item.setItemStatus(ItemStatus.NORMAL);
            else
                item.setItemStatus(ItemStatus.OFF);

            itemRepository.save(item);
        });
    }
}
