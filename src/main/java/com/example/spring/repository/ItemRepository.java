package com.example.spring.repository;

import com.example.spring.entity.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {

    @Query(value = "select * from dbo.Item i where (:price is null or i.price > :price) order by i.id",
            countQuery = "select count(*) from dbo.Item i where (:price is null or i.price > :price)",
            nativeQuery = true)
    Page<Item> findAllByGreatThanPrice(@Param("price") Integer price, Pageable pageable);
}
