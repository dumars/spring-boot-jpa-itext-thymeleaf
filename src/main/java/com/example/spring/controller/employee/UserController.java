package com.example.spring.controller.employee;

import com.example.spring.entity.User;
import com.example.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/employee")
@PreAuthorize("hasAnyRole('ADMIN', 'EMPLOYEE')")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public String index() {
        return "employee/user/list";
    }

    @ResponseBody
    @PostMapping("/users")
    public Page<User> search(String username, String name, Pageable pageable) {
        User user = new User();
        user.setUsername(username);
        user.setName(name);

        ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher
                .matching()
                .withMatcher("username", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withIgnoreNullValues();
        Example<User> example = Example.of(user, caseInsensitiveExampleMatcher);

        return userRepository.findAll(example, pageable);
    }

    @ResponseBody
    @GetMapping("/user/{id}")
    public User findUserById(@PathVariable("id") Long id) {
        return userRepository.findById(id).orElse(new User());
    }
}
