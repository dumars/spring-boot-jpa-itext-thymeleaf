package com.example.spring.controller;

import com.example.spring.entity.Item;
import com.example.spring.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    @GetMapping("/items")
    public String index() {
        return "item/list";
    }

    @ResponseBody
    @PostMapping("/items")
    public Page<Item> query(Integer price, Pageable pageable) {
        return itemRepository.findAllByGreatThanPrice(price, pageable);
    }

    @ResponseBody
    @GetMapping("/item/{id}")
    public Item findItemById(@PathVariable("id") Long id) {
        return itemRepository.findById(id).orElse(new Item());
    }
}
