package com.example.spring.entity;

import com.example.spring.entity.enumeration.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.security.Principal;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Data
@Entity
@Table(name = "users", schema = "dbo")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private Long id;
    @Column(length = 64, unique = true, nullable = false, updatable = false)
    private String username;
    @Column(length = 64)
    private String name;
    @Column(length = 512, nullable = false)
    private String password;
    @Column(length = 128)
    private String email;
    @Column(length = 1024)
    private String description;
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Role role;
    private boolean enabled = true;
    @JsonIgnore
    @Column(name = "credentials_non_expired")
    private boolean credentialsNonExpired = true;
    @JsonIgnore
    @Transient
    private boolean accountNonExpired = true;
    @JsonIgnore
    @Transient
    private boolean accountNonLocked = true;
    @JsonIgnore
    @UpdateTimestamp
    @Column(columnDefinition = "DateTimeOffset")
    private ZonedDateTime lastModifiedDate;
    @JsonIgnore
    @CreationTimestamp
    @Column(updatable = false, columnDefinition = "DateTimeOffset")
    protected ZonedDateTime creationDate;

    public static User of(Principal authentication) {
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
            Object user = token.getPrincipal();
            if (user instanceof User) {
                return (User) user;
            }
        }

        throw new RuntimeException("Can not cast principal to User");
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList((GrantedAuthority) () -> "ROLE_" + Optional.ofNullable(role).orElse(Role.GUEST).name());
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", description='" + description + '\'' +
                ", role=" + role +
                ", enabled=" + enabled +
                ", credentialsNonExpired=" + credentialsNonExpired +
                ", accountNonExpired=" + accountNonExpired +
                ", accountNonLocked=" + accountNonLocked +
                ", creationDate=" + creationDate +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
