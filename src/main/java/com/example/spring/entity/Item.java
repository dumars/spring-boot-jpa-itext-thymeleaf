package com.example.spring.entity;

import com.example.spring.entity.enumeration.ItemStatus;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "item", schema = "dbo")
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private Long id;
    @Column(length = 200)
    private String name;
    @Column
    private Integer price;
    @Column
    private Integer costPrice;
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private ItemStatus itemStatus;
    @Transient
    private String itemStatusLabel;

    public String getItemStatusLabel() {
        if (this.itemStatus != null) {
            return itemStatus.label;
        }
        return null;
    }
}
