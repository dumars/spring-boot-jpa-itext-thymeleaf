package com.example.spring.entity.enumeration;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Role {
    ADMIN("管理員"),
    EMPLOYEE("員工"),
    EXTERNAL_VENDOR("外部廠商"),
    GUEST("遊客");

    public final String label;

    Role(String label) {
        this.label = label;
    }

    public static List<String> toStringList() {
        return Arrays.stream(Role.values()).map(Enum::name).collect(Collectors.toList());
    }
}
