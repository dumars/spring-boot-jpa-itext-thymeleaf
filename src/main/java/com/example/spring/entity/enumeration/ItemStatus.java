package com.example.spring.entity.enumeration;

public enum ItemStatus {

    NORMAL("正常"),
    OFF("下架"),
    REVIEW("審核");

    public final String label;

    ItemStatus(String label) {
        this.label = label;
    }
}
